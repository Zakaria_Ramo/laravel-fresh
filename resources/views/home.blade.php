@extends('master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/all.css') }}">

<div class="container py-5" style="border-radius: 30%;">
  <div class="row">
      <div class="col-"></div>
      <div class="col-8 col-sm-4 px-5">
        <div class="card shadow-lg" border-dotted style="width: 25rem;">
          <div class="card-body">
            <h5 class="card-title" style="font-family:fantasy">The Blue Mosque (Sultan Ahmet Camii)</h5><br>
            <p class="card-text" style="font-family:cursive">Sultan Ahmet Mosque is a mosque until the Ottoman period. It was built in the Ottoman style. One of Turkey's largest mosques.

              Sultan Ahmet Mosque is on the UNESCO World Heritage List. It was taken under protection in 1985 and is in the scope of World Heritage. It is one of the largest mosques among the Sultan Ahmet Mosque. In the courtyard of the mosque, there are places such as baths, tombs, darişşüfa, not places of worship. Sultan Ahmet Mosque is one of the most important works with its construction. In addition, the decorations used in the mosque separate the mosque from places of worship.
              
              Although some elements of the Sultan Ahmet Mosque were not completed after it was built, the mosque managed to reach ten by designing a certain future.</p><br>
            
            
            <div class="row">
              <div class="col-8">
                <a href="#" class="btn border" data-toggle="modal" data-target="#tour">See more</a>  
              </div> 

            </div>
          </div>
          
          <img class="card-img-bottom" src="imgs/sa.jpg" alt="Card image cap">
        </div>
      </div>
      <div class="col-3"></div>
      <div class="col-6 col-sm-4 px-4">
        <div class="card shadow-lg" border-dotted style="width: 25rem;">
          <div class="card-body">
            <h5 class="card-title"style="font-family:fantasy">Hagia Sophia (Ayasofya Camii)</h5><br>
            <p class="card-text" style="font-family:cursive">Hagia Sophia Mosque, which means "Holy Wisdom", has attracted everyone's attention throughout history. Hagia Sophia, which is used as a mosque today, is also known as the largest church built in Istanbul.
              Hagia Sophia, which has an important place in the history of humanity, has been the cradle of culture throughout history. Hagia Sophia Mosque, which was rebuilt 3 times in the same place, is one of the most important structures of Istanbul. Over the years, Hagia Sophia has become the symbol of Istanbul. Hagia Sophia, which is quite magnificent and magnificent, is the largest church built by the Eastern Roman Empire in Istanbul. Hagia Sophia, which was used as a church for years and later as a museum, is still used as a mosque today.</p><br><br>
            <div class="row">
              <div class="col-8">
                <a href="#" class="btn  border" data-toggle="modal" data-target="#tour">See more</a>  
              </div> 

            </div>
          </div>
          <img class="card-img-bottom" src="imgs/aya_sofya.jpg" alt="Card image cap">
        </div>
      </div>
    </div>
    

    <div class="row py-5"> 
      <div class="col-"></div> 
      <div class="col-8 col-sm-4 px-5">
        <div class="card shadow-lg" border-dotted style="width: 25rem;">
          <div class="card-body">
            <h5 class="card-title" style="font-family:fantasy">Topkapi Palace Museum</h5>
            <p class="card-text" style="font-family:cursive">Topkapi Palace is a historical building that was built during the Ottoman Empire. The construction of Topkapi Palace began in 1465 and took 13 years. Its full construction was completed in 1478. It was built in the style of Ottoman architecture. Topkapi Palace is a huge palace where the Ottoman sultans lived. It was built with the aim of using the Ottoman Empire as the administrative state center. Topkapi Palace is a palace built by Fatih Sultan Mehmet. Topkapı Palace was opened and used as a museum for the first time by the Ottoman Sultan Abdülmecit. The architecture of Topkapı Palace is Davud Ağa and Alaüddin.

              Some parts of Topkapı Palace were opened to visitors in 1924. These sections are Baghdad Mansion, Mustafa Pasha mansion, Mecidiye mansion, Hekimbaşı room, Supply room and under the dome. These sections were also repaired at that time.</p>
            <div class="row">
              <div class="col-8">
                <a href="#" class="btn border" data-toggle="modal" data-target="#tour">See more</a>  
              </div> 

            </div>
          </div>
          <img class="card-img-bottom" src="imgs/tk.jpeg" alt="Card image cap">
        </div>
      </div>
      <div class="col-3"></div>
      <div class="col-6 col-sm-4 px-4">
        <div class="card shadow-lg" border-dotted style="width: 25rem;">
          <div class="card-body">
            <h5 class="card-title" style="font-family:fantasy">Grand Bazaar</h5>
            <p class="card-text" style="font-family:cursive">The Grand Bazaar is located in the center of the city of Istanbul, the oldest and largest covered bazaar in the world. Like a giant sized labyrinth, the Grand Bazaar, with its 60 streets and more than three thousand shops, is a unique and must-see center of Istanbul. This site, which resembles a city and is completely covered, has developed and grown over time. The two old buildings with thick walls from the 15th century, covered with a series of domes, became a shopping center in the following centuries by covering the developing streets and making additions. In the past, this was a bazaar where certain professions were located on every street and where the manufacture of handicrafts was under strict control, and commercial ethics and customs were highly respected.</p><br><br>
            <div class="row">
              <div class="col-8">
                <a href="#" class="btn  border" data-toggle="modal" data-target="#tour">See more</a>  
              </div> 
            </div>
          </div>
          <img class="card-img-bottom" src="imgs/gb.jpg" alt="Card image cap">
        </div>
      </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sign In</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="myInput" placeholder="Password">
          </div>        
          <div class="row">
            <div class="col-6">
              <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Remember Me</label>
              </div>
            </div>           
            <div class="col-6">
              <input type="checkbox" onclick="myFunction()">Show Password
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Sign In</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal for tours -->
<div class="modal fade" id="tour" tabindex="-1" role="dialog" aria-labelledby="tourlabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tour">Choose your departure date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      
        <input type="text" id="datepicker" class="datepicker" style="z-index: 10;"/>
        <input type="text" id="datepicker2" class="datepicker" style="z-index: 10;"/>
        <script src="./datepicker.js"></script>
        <script >
          let datepicker = new DatePicker(document.getElementById('datepicker'));
          let datepicker2 = new DatePicker(document.getElementById('datepicker2'));
        </script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

@endsection