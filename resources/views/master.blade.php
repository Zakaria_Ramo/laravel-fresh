<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    
    <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.5/css/foundation.css">
    <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.css"/>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.5/js/vendor/jquery.min.js"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.5/js/foundation.min.js"></script>

</head>

<body>

    <nav class="navbar navbar-expand-lg sticky-top">
        <a class="navbar-brand" href="#"><img src="imgs/logo.png"></a> 
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Collection of nav links, forms, and other content for toggling -->

        <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">	
            @guest	
          <form class="navbar-form form-inline">
            <div class=" search-box">								
              <input type="text" id="search" class="form-control" placeholder="Search for place">
              <span class="input-group-addon"><i class="material-icons"></i></span>
            </div>
          </form>
          <div class="navbar-nav ml-auto">
            <a href="{{ route('home-display') }}" class="nav-item nav-link "><i class="fa fa-home"></i><span>Home</span></a>
            <a href="{{ route('create-place') }}" class="nav-item nav-link"><i class="fa fa-map"></i><span>add new place</span></a> 
            <a href="{{ route('register-user') }}" class="nav-item nav-link"><i class="fa fa-user-plus"></i><span>Register</span></a> 
            <a href="{{ route('login') }}" class="nav-item nav-link"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Login</span></a> 
            
            @else
            <div class="navbar-nav ml-auto">
              <form class="navbar-form form-inline" >
                <div class=" search-box">								
                  <input type="text" id="search" class="form-control" placeholder="Search for place">
                  <span class="input-group-addon"><i class="material-icons"></i></span>
                </div>
              </form>
              <a href="{{ route('create-place') }}" class="nav-item nav-link"><i class="fa fa-map"></i><span>add new place</span></a> 
              <a href="#" class="nav-item nav-link active"><i class="fa fa-home"></i><span>Home</span></a>
              <a href="{{ route('signout') }}" class="nav-item nav-link"><i class="fa fa-sign-out"></i><span>Signout</span></a> 
            </div>
          </div>

          @endguest
        </div>
    </nav>

      @if( Session::has( 'success' ))
        <div  class = "alert shadow-lg " data-closable>   
          <strong>
            {{ Session::get( 'success' ) }}</strong>
          <button style="color: white"class = "close-button" data-close aria-label = "Dismiss alert">×</button>
        </div>
        @elseif( Session::has( 'warning' ))
        <div style="background-color: #B22222"
                    class = "alert shadow-lg" data-closable>   
          <strong>
            {{ Session::get( 'warning' ) }} <!-- here to 'withWarning()' -->
          </strong>
          <button style="color: white"class = "close-button" data-close aria-label = "Dismiss alert">×</button>
        </div>
        
      @endif


    @yield('content')
        
    <div class="social-media ">
      <div class="container">
        <div class="row">
             <div class="col-2"></div>
            <div class="col-2">
              <a href="#" class="nav-item nav-link"><i class="fa fa-facebook"></i></a>
            </div>  
            <div class="col-3">
              <a href="#" class="nav-item nav-link"><i class="fa fa-twitter"></i></a>
            </div> 
            <div class="col-2">
              <a href="#" class="nav-item nav-link"><i class="fa fa-google-plus"></i></a>
            </div> 
            <div class="col-1">
              <a href="#" class="nav-item nav-link"><i class="fa fa-pinterest-p"></i></a>
            </div> 
            <div class="col-2"></div>
        </div> 
      </div>
    </div>
  <footer class="footer">
      <div class="container text-white">
          <div class="row py-5">
              <div class="col-2"></div>
              <div class="col-lg-3 mb-5 mb-lg-0">
                <a class="nav-link active text-white" aria-current="page" href="#">
                  Privacy & Cookie Policy</a>
              </div>
              
              <div class="col-lg-3 mb-5 mb-lg-0">
                  <a class="nav-link active text-white" aria-current="page" href="#">
                    Legal Notice</a>  
              </div>
              
              <div class="col-lg-3">
                <a class="nav-link active text-white" aria-current="page" href="#">About Us</a>   
              </div>           
          </div>
      </div>           
  </footer> 

</body>

</html>