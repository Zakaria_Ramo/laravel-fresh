@extends('master')

@section('content')

    <form action="{{ route('store-place')}}" method="POST">
        @csrf
        @include('posts.partials.form')
        <div>
            <input type="submit" value="create">
        </div>
    </form>
@endsection
