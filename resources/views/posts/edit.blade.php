@extends('master')

@section('content')

    <form action="{{ route('update-place', ['post' => $post->id]) }}" method="POST">
        @csrf
        @method('PUT')
        @include('posts.partials.form')
        <div>
            <input type="submit" value="Update">
        </div>
    </form>
@endsection
