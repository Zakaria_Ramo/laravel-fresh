@extends('master')

@section('content')
    <h1> {{$post['title']}}</h1>
    <h2>{{$post['id']}}</h2>
    <div>
        <p>
            {{$post['content']}}
        </p>
    </div>
    @auth()
    <form action="{{route('edit-place',['post'=>$post->id])}}" method="POST">
        @csrf
        @method('PUT')
        <input type="submit" value="Edit!">
    </form>
    @endauth
@endsection
