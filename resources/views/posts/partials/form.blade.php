@csrf
<div>
    <input type="text" name="title" value="{{old('title', optional($post ?? null)->title)}}">
</div>
@error('title')
<div>{{$message}}</div>
@enderror

<div>
    <input type="text" name="type" value="{{old('type', optional($post ?? null)->type)}}">
</div>

<div>
    <input type="text" name="location" value="{{old('location', optional($post ?? null)->location)}}">
</div>

<div>
    <textarea name="description" id="" cols="30" rows="5" >{{old('description',optional($post ?? null)->description)}}</textarea>
</div>

@if($errors->any())
    <div>
        <ul>
            @foreach($errors->all() as $error)
                <li> {{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif


