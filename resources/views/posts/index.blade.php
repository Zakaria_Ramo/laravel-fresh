@extends('master')
<!-- Section-->
@section('content')

    @if(session('status'))
        <div style="color:green">
            {{ session('status')}}
        </div>
    @endif
    <table>
        @foreach($posts as $key => $post)
            <tr>
                <td>{{$post['title']}}</td>
                <td>{{$post['description']}}</td>
                <td>{{$post['user_id']}}</td>

                

                @auth()
                    <td>
                        <form action="{{route('delete-place',['post'=>$post->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="bg-dark text-white" value="Delete">
                        </form>
                    </td>
                    <td>
                        <form action="{{route('edit-place',['post'=>$post->id])}}" method="GET">
                            @csrf
                            @method('GET')
                            <input type="submit" value="Edit!">
                        </form>
                    </td>
                @endauth
            </tr>
        @endforeach
    </table>
@endsection
