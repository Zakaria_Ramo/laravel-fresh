<?php

use App\Http\Controllers\PlaceController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
Route::resource('Place', PlaceController::class);
Route::resource('Image', ImageController::class);
Route::resource('Comment', CommentController::class);
Route::resource('Rating', RatingController::class);
*/


// Public routes



Route::get('/places/search/{title}', [PlaceController::class, 'search']);

Route::get('/places', [PlaceController::class, 'index']);
Route::get('/comments', [CommentController::class, 'index']);
Route::get('/images', [ImageController::class, 'index']);
Route::get('/ratings', [RatingController::class, 'index']); 

Route::get('/places/{id}', [PlaceController::class, 'show']);
Route::get('/comments/{id}', [CommentController::class, 'show']);
Route::get('/images/{id}', [ImageController::class, 'show']);
Route::get('/ratings/{id}', [RatingController::class, 'show']); 

Route::post('/register', [AuthController::class, 'register']);

Route::get('/users', [AuthController::class, 'index']);

Route::post('/images', [ImageController::class, 'store']);

// Protected routes

Route::group(['middleware' => ['auth:sanctum']], function () {


    Route::delete('/images/{id}', [ImageController::class, 'destroy']);

    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/login', [AuthController::class, 'login']);

    Route::post('/places', [PlaceController::class, 'store']);
    Route::post('/comments', [CommentController::class, 'store']);
    
    Route::post('/ratings', [RatingController::class, 'store']);
    
    Route::delete('/places/{id}', [PlaceController::class, 'destroy']);
    Route::delete('/comments/{id}', [CommentController::class, 'destroy']);
    
    Route::delete('/ratings/{id}', [RatingController::class, 'destroy']); 

    Route::put('/places/{id}', [PlaceController::class, 'update']);
    Route::put('/comments/{id}', [CommentController::class, 'update']);
    Route::put('/images/{id}', [ImageController::class, 'update']);
    Route::put('/ratings/{id}', [RatingController::class, 'update']); 
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
