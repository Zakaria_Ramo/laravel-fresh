<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('home');
});

//Route::get('/users', [CustomAuthController::class, 'show1']);

Route::get('welcome', function () {
    return view('welcome');
});

Route::get('master', [CustomAuthController::class, 'master']); 
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');
Route::get('home', [PlaceController::class, 'home'])->name('home-display'); 

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/places/create', [PlaceController::class, 'create'])->name('create-place');
    Route::post('/places', [PlaceController::class, 'store'])->name('store-place');
    //Route::get('/places', [PlaceController::class, 'index'])->name('show-places');
    Route::get('/places/edit/{post}', [PlaceController::class, 'edit'])->name('edit-place');
    Route::put('/places/update/{post}', [PlaceController::class, 'update'])->name('update-place');
    Route::delete('/places/{post}', [PlaceController::class, 'destroy'])->name('delete-place');
    Route::get('/places', [PlaceController::class, 'index'])->name('place-index');

});