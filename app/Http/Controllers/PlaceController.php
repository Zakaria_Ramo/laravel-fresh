<?php

namespace App\Http\Controllers;
use App\Models\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    public function home()
    {
        return view('home');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function index()
    {
        return view('posts.index',['posts'=>Place::all()]);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function create(){
        return view('posts.create');

    }
    public function store(Request $request)
    {
        $request->validate([
            'title' =>'required',
            'type' =>'required',
            'location' =>'required',
            'description' =>'required',
            
        ]);
        Place::create($request->all());
        return redirect('/places');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Place::find($id)->with('Image');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $post= Place::find($id);
        
        return view('posts.edit', compact('post'));
    }

    public function update(Request $request, $post)
    {
        $place = Place::find($post);
        $place->update($request->all());
        return redirect('/places');
    }

    /**
     * search for title of place.
     * 
     * @param  string  $title
     * @return \Illuminate\Http\Response
     */
    public function search($title)
    {
        
        return Place::where('title', 'like', '%'.$title.'%')->get();
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post)
    {   
        Place::destroy($post);
        return redirect('/places');
        
    }
}
